'''
Parses the collision layer from the Tiled map, and creates a sorted matrix with the collision values.
'''
class CollisionMapHelper:

    COLLISION_LAYER_DEFINITIONS = "objects"

    # Collision spec data
    COLLISION_LAYER_X = "x"
    COLLISION_LAYER_Y = "y"
    COLLISION_LAYER_WIDTH = "width"
    COLLISION_LAYER_HEIGHT = "height"
    COLLISION_LAYER_TYPE = "type"

    # Collision definitions indexes
    COLLISION_X = 0
    COLLISION_Y = 1
    COLLISION_WIDTH = 2
    COLLISION_HEIGHT = 3
    COLLISION_TYPE = 4

    def __init__(self, collisionLayerData):
        self.collisionDefinitions = []

        for collisionObject in collisionLayerData[CollisionMapHelper.COLLISION_LAYER_DEFINITIONS]:
            definition = []

            definition.append(int(collisionObject[CollisionMapHelper.COLLISION_LAYER_X]))
            definition.append(int(collisionObject[CollisionMapHelper.COLLISION_LAYER_Y]))
            definition.append(int(collisionObject[CollisionMapHelper.COLLISION_LAYER_WIDTH]))
            definition.append(int(collisionObject[CollisionMapHelper.COLLISION_LAYER_HEIGHT]))
            definition.append(int(collisionObject[CollisionMapHelper.COLLISION_LAYER_TYPE]))

            self.collisionDefinitions.append(definition)

    def getCollisionDefinitions(self):
        return self.collisionDefinitions
