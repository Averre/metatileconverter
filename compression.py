
class SimpleRLECompression:

    '''
    Takes a multidimensional list containing rows of map data.
    '''
    def compress(self, rawData):
        compressedDataList = []
        flatList = self.convertMatrixToFlatList(rawData)
        actualIndex = 0

        for index, item in enumerate(flatList):
            if index != actualIndex:
                continue

            matchingItemsAmount = self.countFollowingMatchingItems(item, flatList[index+1:])

            if matchingItemsAmount != 0:
                compressedDataList.append(matchingItemsAmount + 1)
                compressedDataList.append(item)
                actualIndex += matchingItemsAmount + 1
            else:
                compressedDataList.append(1)
                compressedDataList.append(item)
                actualIndex += 1

        return bytearray(compressedDataList)


    def countFollowingMatchingItems(self, item, subList):
        matchingItems = 0

        for nextItem in subList:
            if item != nextItem:
                break

            matchingItems += 1

        return matchingItems


    def convertMatrixToFlatList(self, matrix):
        flatList = []

        for list in matrix:
            for item in list:
                flatList.append(item)

        return flatList
