from png import Reader
from png import Writer
from metatile import Metatile
from maptileConverter import MaptileConverter
from palette import Palette

class Converter:
    # Reader constants
    WIDTH = 0
    HEIGHT = 1
    PIXELS = 2
    METADATA = 3
    PALETTE = "palette"

    def __init__(self):
        self.extractCornerTiles()
        self.extractUniqueTiles()
        #self.drawTilemap()

        self.writeUniqueTilesFile()
        self.writePaletteFile()

        maptileConverter = MaptileConverter()
        #maptileConverter.convertMap("/Users/ricardo/Programmering/SMS/Projects/Platformer/single_screen_map.json", self.metatiles)
        #maptileConverter.convertMap("/Users/ricardo/Programmering/SMS/Projects/Platformer/Gimmick-map.json",self.metatiles)
        #maptileConverter.convertMap("/Users/ricardo/Programmering/SMS/Projects/Platformer/Gimmick-map_vertical.json",self.metatiles)
        #maptileConverter.convertMap("/Users/ricardo/Programmering/SMS/Projects/Platformer/Gimmick-map-complete.json",
        #                            self.metatiles)
        maptileConverter.convertMap("/Users/ricardo/workspace/SMS/HorrorDemo/src/rooms/mainroom/res/mainroom.json", self.metatiles)

    def extractCornerTiles(self):
        #metatilesFile = "/Users/ricardo/Programmering/SMS/Projects/Platformer/metatiles.png"
        #metatilesFile = "/Users/ricardo/Programmering/SMS/Projects/Platformer/Gimmick-converted-spritesheet.png"
        #metatilesFile = "/Users/ricardo/Programmering/SMS/Projects/Platformer/Gimmick-converted-partial_spritesheet.png"
        metatilesFile = "/Users/ricardo/workspace/SMS/HorrorDemo/src/rooms/mainroom/res/mainroom_metatiles.png"

        reader = Reader(filename=metatilesFile)
        self.metatilesFileContents = reader.read()

        self.sheetWidth = int(self.metatilesFileContents[Converter.WIDTH])
        self.sheetHeight = int(self.metatilesFileContents[Converter.HEIGHT])
        self.validateSize(self.sheetWidth, self.sheetHeight)

        # self.metatiles = []
        self.metatiles = {}

        self.pixels = self.metatilesFileContents[Converter.PIXELS]

        rowIndex = 0
        metatileIndex = 0
        metatileRowIndex = 0
        metatileColumnIndex = 0
        metatilesPerRow = int(self.sheetWidth/16)

        for row in self.pixels:
            pixelIndex = 0

            for pixel in row:
                metatile = None

                metatile = self.metatiles.get(metatileIndex)

                if metatile is None:
                    metatile = Metatile()
                    self.metatiles[metatile.id] = metatile

                paletteIndex = pixel

                if paletteIndex > 15:
                    paletteIndex -= 16

                metatile.appendPixelColour(metatileRowIndex, metatileColumnIndex, paletteIndex)

                pixelIndex += 1

                # Restart the metatile column index counter when reaching 16.
                metatileColumnIndex += 1
                metatileColumnIndex &= 0x0F

                if metatileColumnIndex == 0:
                    metatileIndex += 1

            rowIndex += 1
            metatileIndex = int(rowIndex/16) * metatilesPerRow

            metatileRowIndex += 1
            metatileRowIndex &= 0x0F


    def extractUniqueTiles(self):
        self.uniqueTiles = []

        for metatile in self.metatiles.values():
            for cornertile in metatile.cornerTiles:
                uniqueIndex = self.getUniqueTileIndex(self.uniqueTiles, cornertile)

                # Unique tile has not been previously found.
                # Add it to the list.
                if uniqueIndex == -1:
                    uniqueIndex = len(self.uniqueTiles)
                    self.uniqueTiles.append(cornertile)

                cornertile.appendIndex(uniqueIndex)
                metatile.tilesOrder.append(uniqueIndex)

        # Validate the size of the unique tiles.
        # We are restricting the game to only use the lower 256 tile slots.
        # Sprites will utilize the upper 256 tile slots.
        uniqueTilesAmount = len(self.uniqueTiles)
        if uniqueTilesAmount > 256:
            raise Exception("Generated unique tiles surpass the allowed amount of 256. Generated {} unique 8x8px tiles.", uniqueTilesAmount)



    def getUniqueTileIndex(self, uniqueTiles, cornerTile):
        for uniqueTile in uniqueTiles:
            try:
                sameTileResult = self.isSameTile(uniqueTile, cornerTile)
            except IndexError:
                print("Error")

            if sameTileResult:
                return uniqueTile.index

        return -1

        '''
        for index, uniqueTile in enumerate(uniqueTiles):
            sameTileResult = self.isSameTile(uniqueTile, cornerTile)

            if sameTileResult:
                return index

        return -1
        '''


    def isSameTile(self, uniqueTile, cornerTile):
        for index, row in enumerate(uniqueTile.rows):
            cornertileRow = cornerTile.getRow(index)

            rowResult = self.isSameRow(row, cornertileRow)

            if not rowResult:
                return False

        return True


    def isSameRow(self, uniqueTileRow, cornerTileRow):
        for index, pixel in enumerate(uniqueTileRow):
            cornerTileRowPixel = cornerTileRow[index]

            if pixel != cornerTileRowPixel:
                return False

        return True


    def drawTilemap(self):
        metadata = self.metatilesFileContents[Converter.METADATA]
        palette = metadata[Converter.PALETTE]
        print(palette)

        tilemapFile = open('/Users/ricardo/Temp/generated_tilemap.png', 'w')
        #png.Writer()


    def validateSize(self, width, height):
        if int(width) % 16 != 0 or int(height) % 16 != 0:
            raise Exception("Incorrect metatilesheet dimensions! - width: {}; height: {}", width, height)


    def writeUniqueTilesFile(self):
        tilesAssemblyFilePath = "/Users/ricardo/Temp/uniqueTiles.inc"
        tilesAssemblyFilePath = open(tilesAssemblyFilePath, "w")

        for tile in self.uniqueTiles:
            tilesAssemblyFilePath.write(tile.toVDPFormat() + "\n")

        tilesAssemblyFilePath.close()


    def writePaletteFile(self):
        self.palette = Palette(self.metatilesFileContents[self.METADATA][self.PALETTE])
        paletteAssemblyFilePath = "/Users/ricardo/Temp/palette.inc"
        paletteAssemblyFilePath = open(paletteAssemblyFilePath, "w")
        paletteAssemblyFilePath.write(self.palette.toVDPFormat())
        paletteAssemblyFilePath.close()


if __name__ == "__main__":
    mainApp = Converter()
