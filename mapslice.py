from maptile import MapTile
from tiledMaptileUtil import TiledMaptileUtil
from collisionmap import CollisionMapHelper

class MapSlice:

    # Tile data
    LAYER_TILE_DATA = "data"
    LAYER_TILE_PROPERTIES = "properties"
    LAYER_TILE_PROPERTIES_PALETTE = "Palette"
    LAYER_TILE_PROPERTIES_FOREGROUND = "Foreground"

    # Slices spec data
    SLICES_NAME = "name"
    SLICES_Y = "y"
    SLICES_X = "x"
    SLICES_WIDTH = "width"
    SLICES_HEIGHT = "height"
    SLICES_REVERSE = "Reverse"
    SLICES_PROPERTIES = "properties"

    # Scroll orientations
    SLICES_SCROLL_HORIZONTAL = "Horizontal"
    SLICES_SCROLL_VERTICAL = "Vertical"

    def __init__(self, sliceSpec, mapWidthAsTiles, mapHeightAsTiles):
        self.sliceSpec = sliceSpec
        self.mapWidthAsTiles = int(mapWidthAsTiles)
        self.mapHeightAsTiles = int(mapHeightAsTiles)

        # Indicates if the game scrolls in the "reverse" direction.
        # Normal scroll is when the player moves from:
        # Left->Right; Top->Bottom
        self.reverseScroll = bool(self.sliceSpec[MapSlice.SLICES_PROPERTIES][MapSlice.SLICES_REVERSE])
        self.scrollOrientation = None

        # List with lists for every layer containing maptile objects for every position in the slice.
        self.data = []

        self.width = int(self.sliceSpec[MapSlice.SLICES_WIDTH])
        self.height = int(self.sliceSpec[MapSlice.SLICES_HEIGHT])
        self.startX = int(self.sliceSpec[MapSlice.SLICES_X])
        self.startY = int(self.sliceSpec[MapSlice.SLICES_Y])

        if self.height > self.width:
            self.scrollOrientation = MapSlice.SLICES_SCROLL_VERTICAL
        else:
            self.scrollOrientation = MapSlice.SLICES_SCROLL_HORIZONTAL

        self.maptilesInRow = int(self.width / 16)
        self.maptilesInColumn = int(self.height / 16)
        self.maptilesInSlice = self.maptilesInRow * self.maptilesInColumn


    def parseTilelayer(self, tilelayerData, maptiles, metatiles):
        layerPalette = int(tilelayerData[MapSlice.LAYER_TILE_PROPERTIES][MapSlice.LAYER_TILE_PROPERTIES_PALETTE])
        layerForeground = bool(tilelayerData[MapSlice.LAYER_TILE_PROPERTIES][MapSlice.LAYER_TILE_PROPERTIES_FOREGROUND])
        foreground = 0

        if layerForeground:
            foreground = 1

        currentMaptileIndex = int(self.startX / 16) + (int(self.startY / 16) * self.mapWidthAsTiles)

        rowIndex = 0
        sliceMaptiles = {}
        sliceMaptiles[rowIndex] = []

        for x in range(0, self.maptilesInSlice):
            metatileIndex = int(tilelayerData[MapSlice.LAYER_TILE_DATA][currentMaptileIndex])

            modflag = None

            '''
            If the id is less than 1000, it is safe to assume that it is the actual ID
            of a tile in the tilemap.
            '''
            if metatileIndex < 1000:
                modflag = TiledMaptileUtil.createModFlag(foreground, layerPalette, 0, 0)
                horizontallyFlipped = False
                verticallyFlipped = False
            else:
                horizontallyFlipped = TiledMaptileUtil.isHorizontallyFlipped(metatileIndex)
                verticallyFlipped = TiledMaptileUtil.isVerticallyFlipped(metatileIndex)
                modflag = TiledMaptileUtil.createModFlag(foreground, layerPalette, verticallyFlipped, horizontallyFlipped)

                metatileIndex = TiledMaptileUtil.getActualGlobalId(metatileIndex)

            # must subtract 1 from the ID, because .tmx IDs start at 1.
            metatileIndex -= 1

            if metatileIndex != -1:
                metatileIndex = self.getActualMetatileIndex(metatileIndex, horizontallyFlipped, verticallyFlipped, metatiles)

                # TODO: Replace the value 0 for type, with a value from a proper lookup in the collision layer.
                maptile = self.getMapTile(metatileIndex, modflag, 0, maptiles)
                sliceMaptiles[rowIndex].append(maptile)
            else:
                # If the metatileIndex is -1, then no metatile hasn't been placed on that slot for that layer.
                # There should be a metatile in that slot on another layer.
                sliceMaptiles[rowIndex].append(None)

            if (x + 1) % self.maptilesInRow == 0:
                if (x + 1) == self.maptilesInSlice:
                    break

                rowIndex += 1
                sliceMaptiles[rowIndex] = []
                currentMaptileIndex += (self.mapWidthAsTiles - self.maptilesInRow + 1)
            else:
                currentMaptileIndex += 1

        self.data.append(sliceMaptiles)


    '''
    Uses the given metatile index which is a "base" index. That means it is the index to a metatile
    that has not been flipped. And retrieves the correct index based on the flip flags.
    '''
    def getActualMetatileIndex(self, metatileIndex, hFlipped, vFlipped, metatiles):
        for key, value in metatiles.items():
            if metatileIndex == key:
                return value.getMetatileIndex(hFlipped, vFlipped)

        raise Exception("metatileIndex {} does not exist among the existing metatiles!", str(metatileIndex))


    def getMapTile(self, metatileIndex, modFlag, type, existingMaptiles):
        for existingMaptile in existingMaptiles:
            if existingMaptile.equals(metatileIndex, modFlag, type):
                return existingMaptile

        maptile = MapTile(metatileIndex, modFlag, type)
        existingMaptiles.append(maptile)

        return maptile

    '''
    This method returns a dict where every entry contains a list of maptiles.
    The list are formatted and ordered depending of the orientation of the map slice,
    and also of the scroll direction of the slice (if the player moves from left->right;top->bottom or reverse).
    
    Left -> Right
    Lists contain columns with maptiles.
    From left to right.
    
    Right -> Left
    Lists contain columns with maptiles.
    From right to left.
    
    Top -> Bottom
    Lists contain rows with maptiles.
    From top to bottom.
    
    Bottom -> Top
    Lists contain rows with maptiles.
    From bottom to top.  
    '''
    def getMap(self):
        unorderedMergedData = self.mergeLayers()
        orderedMergedData = self.orderMergedData(unorderedMergedData)

        if self.scrollOrientation == MapSlice.SLICES_SCROLL_VERTICAL:
            if not self.reverseScroll:
                return orderedMergedData
            else:
                return list(reversed(orderedMergedData))
        else:
            # Scroll orientation is horizontal.
            # Convert the map to lists of columns.
            horizontallyOrderedMergedData = self.convertMergedDataToHorizontalFormat(orderedMergedData)

            if not self.reverseScroll:
                return horizontallyOrderedMergedData
            else:
                return list(reversed(horizontallyOrderedMergedData))


    '''
    Takes a dict with merged data as input and returns a list with lists in the correct order.
    '''
    def orderMergedData(self, mergedData):
        orderedMergedData = []

        for x in range(0, len(mergedData)):
            orderedMergedData.append(mergedData[x])

        return orderedMergedData


    '''
    Takes a list with map data ordered with rows from top to bottom, and returns the same data
    but as lists ordered in columns.  
    '''
    def convertMergedDataToHorizontalFormat(self, orderedMergedData):
        horizontalFormat = []

        for columnIndex in range(0, len(orderedMergedData[0])):
            column = []

            for row in orderedMergedData:
                column.append(row[columnIndex])

            horizontalFormat.append(column)

        return horizontalFormat


    def mergeLayers(self):
        mergedData = {}

        for layer in self.data:
            for rowIndex, layerRow in layer.items():
                mergedRow = mergedData.get(rowIndex)

                if mergedRow is None:
                    mergedRow = []

                for columnIndex, layerMaptile in enumerate(layerRow):
                    try:
                        existingMetatile = mergedRow[columnIndex]

                        if layerMaptile is not None and existingMetatile is not None:
                            raise Exception("Failed merge of slice {}. Duplicate maptile in row {}; column {}.", self.sliceSpec[MapSlice.SLICES_NAME], rowIndex, columnIndex)
                        elif (layerMaptile is not None and existingMetatile is None) or (layerMaptile is None and existingMetatile is None):
                            mergedRow[columnIndex] = layerMaptile
                    except IndexError:
                        # No maptile exists yet at the given position.
                        # Add the current layer maptile.
                        mergedRow.append(layerMaptile)

                mergedData[rowIndex] = mergedRow

        self.verifyMergedData(mergedData)

        return mergedData


    '''
    Verifies that there are no None values in the data.
    '''
    def verifyMergedData(self, mergedData):
        for rowIndex in range(0, len(mergedData)):
            row = mergedData[rowIndex]

            if row is None:
                raise Exception("Missing row data for row {}, in slice {}.", rowIndex, self.sliceSpec[MapSlice.SLICES_NAME])

            for columnIndex, maptile in enumerate(row):
                if maptile is None:
                    raise Exception("Missing maptile from slice {}. Row {}; Column {}", self.sliceSpec[MapSlice.SLICES_NAME], rowIndex, columnIndex)


    def getCollisionMap(self, collisionDefinitions):
        orderedCollisionMap = self.orderedCollisionMap(collisionDefinitions)

        if self.scrollOrientation == MapSlice.SLICES_SCROLL_VERTICAL:
            if not self.reverseScroll:
                return orderedCollisionMap
            else:
                return list(reversed(orderedCollisionMap))
        else:
            # Scroll orientation is horizontal.
            # Convert the map to lists of columns.
            horizontalCollisionMap = self.convertMergedDataToHorizontalFormat(orderedCollisionMap)

            if not self.reverseScroll:
                return horizontalCollisionMap
            else:
                return list(reversed(horizontalCollisionMap))

    '''
    Creates a matrix with rows containing the collision definitions for every maptile in the mapslice. 
    '''
    def orderedCollisionMap(self, collisionDefinitions):
        collisionmap = []

        currentTileX = self.startX
        currentTileY = self.startY

        for x in range(0, self.maptilesInColumn):
            rowCollisionMap = []

            for x in range(0, self.maptilesInRow):
                collisionValue = self.getCollisionValue(currentTileX, currentTileY, 16, 16, collisionDefinitions)
                rowCollisionMap.append(collisionValue)
                currentTileX += 16

            collisionmap.append(rowCollisionMap)
            currentTileX = self.startX
            currentTileY += 16

        return collisionmap



    '''
    Tests if the given tile is included in a collision box.
    If true the value of the collision box definition is returned.

    A tile can be included whithin several collision boxes. In such a case the smallest box has the highest priority.
    '''
    def getCollisionValue(self, tileX, tileY, tileWidth, tileHeight, collisionDefinitions):
        matchingCollisionBox = (0, 0)

        for definition in collisionDefinitions:
            if tileX >= definition[CollisionMapHelper.COLLISION_X]\
                    and (tileX + tileWidth) <= (definition[CollisionMapHelper.COLLISION_X] + definition[CollisionMapHelper.COLLISION_WIDTH])\
                    and tileY >= definition[CollisionMapHelper.COLLISION_Y]\
                    and (tileY + tileHeight) <= (definition[CollisionMapHelper.COLLISION_Y] + definition[CollisionMapHelper.COLLISION_HEIGHT]):

                collisionArea = definition[CollisionMapHelper.COLLISION_WIDTH] * definition[CollisionMapHelper.COLLISION_HEIGHT]

                if matchingCollisionBox[0] == 0 or collisionArea < matchingCollisionBox[0]:
                    matchingCollisionBox = (collisionArea, definition[CollisionMapHelper.COLLISION_TYPE])

        return matchingCollisionBox[1]
