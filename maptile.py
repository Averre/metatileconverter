
class MapTile:

    INCREMENTING_ID = 0

    def __init__(self, metatileIndex, modFlag, type):
        self.id = MapTile.INCREMENTING_ID
        MapTile.INCREMENTING_ID += 1

        self.metatileIndex = metatileIndex

        # The modflag selects the palette, foreground/background and vertical/horizontal flip.
        self.modFlag = modFlag

        # The type signifies if the maptile is empty, solid, damaging etc.
        self.type = type


    def equals(self, metatileIndex, modFlag, type):
        if metatileIndex != self.metatileIndex:
            return False

        if modFlag != self.modFlag:
            return False

        if type != self.type:
            return False

        return True
