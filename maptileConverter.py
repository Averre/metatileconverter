import json
from mapslice import MapSlice
from maptile import MapTile
from collisionmap import CollisionMapHelper
from compression import SimpleRLECompression

class MaptileConverter:

    # General map data
    MAP_WIDTH = "width"
    MAP_HEIGHT = "height"

    LAYER_NAME = "name"
    LAYERS = "layers"

    # Tile data
    LAYER_TILE_DATA = "data"

    # Layer names
    LAYER_NAME_SLICES = "Slices"
    LAYER_NAME_BACKGROUND_0 = "Background 0"    # Background with palette 0
    LAYER_NAME_BACKGROUND_1 = "Background 1"    # Background with palette 1
    LAYER_NAME_FOREGROUND_0 = "Foreground 0"    # Foreground with palette 0
    LAYER_NAME_FOREGROUND_1 = "Foreground 1"    # Foreground with palette 1
    LAYER_NAME_ALTERNATE = "Alternate"
    LAYER_NAME_COLLISION = "Collision"

    # Layer Types
    LAYER_TYPE = "type"
    LAYER_TYPE_TILE = "tilelayer"
    LAYER_TYPE_OBJECTGROUP = "objectgroup"

    # Slices data
    SLICES = "objects"
    SLICES_NAME = "name"

    def convertMap(self, mapFilepath, metatiles):
        mapData = json.load(open(mapFilepath))
        mapWidth = int(mapData[MaptileConverter.MAP_WIDTH])
        mapHeight = int(mapData[MaptileConverter.MAP_HEIGHT])

        self.maptiles = []
        self.mapSlices = []
        self.collisionLayer = None
        alternateTilelayer = None
        tilelayers = []

        for layer in mapData[MaptileConverter.LAYERS]:
            if layer[MaptileConverter.LAYER_TYPE] == MaptileConverter.LAYER_TYPE_OBJECTGROUP:
                if layer[MaptileConverter.LAYER_NAME] == MaptileConverter.LAYER_NAME_SLICES:
                    self.extractAndSortSlices(layer[MaptileConverter.SLICES])
                elif layer[MaptileConverter.LAYER_NAME] == MaptileConverter.LAYER_NAME_COLLISION:
                    self.collisionLayer = layer
            elif layer[MaptileConverter.LAYER_TYPE] == MaptileConverter.LAYER_TYPE_TILE:
                if layer[MaptileConverter.LAYER_NAME] == MaptileConverter.LAYER_NAME_ALTERNATE:
                    alternateTilelayer = layer
                else:
                    tilelayers.append(layer)

        collisionMapHelper = CollisionMapHelper(self.collisionLayer)

        for sliceIndex, sliceData in enumerate(self.slices):
            mapSlice = MapSlice(sliceData, mapWidth, mapHeight)

            for layer in tilelayers:
                mapSlice.parseTilelayer(layer, self.maptiles, metatiles)
                self.mapSlices.append(mapSlice)

            sliceMap = mapSlice.getMap()
            '''
            for row in sliceMap:
                for maptile in row:
                    print(str(maptile.id) + " ", end="", flush=True)
                print("")
            '''

            '''
            Write map to file.
            '''

            sliceMapAssemblyFile = "/Users/ricardo/Temp/mapSlice_" + str(sliceIndex) + ".inc"
            sliceMapAssemblyFile = open(sliceMapAssemblyFile, "w")

            for row in sliceMap:
                rowIds = []
                for maptile in row:
                    rowIds.append(maptile.id)
                sliceMapAssemblyFile.write(".DB " + ",".join(str(id) for id in rowIds) + "\n")

            sliceMapAssemblyFile.close()

            '''
            Write collision map to file
            '''

            sliceCollisionMap = mapSlice.getCollisionMap(collisionMapHelper.getCollisionDefinitions())

            sliceCollisionMapAssemblyFile = "/Users/ricardo/Temp/mapSlice_" + str(sliceIndex) + "_collision.inc"
            sliceCollisionMapAssemblyFile = open(sliceCollisionMapAssemblyFile, "w")

            for row in sliceCollisionMap:
                rowCollisionDefinitions = []
                for collisionDefinition in row:
                    rowCollisionDefinitions.append(collisionDefinition)

                sliceCollisionMapAssemblyFile.write(".DB " + ",".join(str(definition) for definition in rowCollisionDefinitions) + "\n")

            sliceCollisionMapAssemblyFile.close()

            '''
            !!!!TEST!!!!
            '''
            for row in sliceCollisionMap:
                for collision in row:
                    print(str(collision) + " ", end="", flush=True)
                print("")

            print("")

            compression = SimpleRLECompression()
            compressedCollisionData = compression.compress(sliceCollisionMap)
            for collisionData in compressedCollisionData:
                print(str(collisionData) + " ", end="", flush=True)

            print("")
            uncompressedSize =  0
            for rowCollisionMap in sliceCollisionMap:
                uncompressedSize += len(rowCollisionMap)

            print("Compressed: " + str(uncompressedSize) + " bytes")
            print("Uncompressed: " + str(len(compressedCollisionData)) + " bytes")
            print("Compression rate: " + str(len(compressedCollisionData) / uncompressedSize * 100) + " %")
            print("")

            compressedSliceCollisionMapFile = "/Users/ricardo/Temp/mapSlice_" + str(sliceIndex) + "_collision.bin"
            compressedSliceCollisionMapFile = open(compressedSliceCollisionMapFile, "w+b")
            compressedSliceCollisionMapFile.write(compressedCollisionData)
            compressedSliceCollisionMapFile.close()

            print(bytes(compressedCollisionData))
            print("")


        # Write maptiles, metatile array
        sortedMaptiles = self.sortMaptiles(self.maptiles)
        mapTileMetatileIndexAssembly = "/Users/ricardo/Temp/mapTileMetatileArray.inc"
        mapTileMetatileIndexAssembly = open(mapTileMetatileIndexAssembly, "w")

        metatileIDs = []
        for maptile in sortedMaptiles:
            metatileIDs.append(maptile.metatileIndex)

        mapTileMetatileIndexAssembly.write(".DB " + ",".join(str(id) for id in metatileIDs) + "\n")
        mapTileMetatileIndexAssembly.close()


        # Write maptiles, modflags array
        mapTileModFlagsAssembly = "/Users/ricardo/Temp/mapTileModflagsArray.inc"
        mapTileModFlagsAssembly = open(mapTileModFlagsAssembly, "w")

        modFlags = []
        for maptile in sortedMaptiles:
            modFlags.append("{:02x}".format(int(str(maptile.modFlag), 16)))

        mapTileModFlagsAssembly.write(".DB " + ",".join(str(modFlag) for modFlag in modFlags) + "\n")
        mapTileModFlagsAssembly.close()


        # Write metatiles arrays
        sortedMetatiles = self.extractAllActualMetatilesAndSort(metatiles)

        # Write metatiles, top left corner array
        topLeftCornerTileIndexes = []
        topRightCornerTileIndexes = []
        bottomLeftCornerTileIndexes = []
        bottomRightCornerTileIndexes = []

        for metatile in sortedMetatiles:
            topLeftCornerTileIndexes.append(metatile.tilesOrder[0])
            topRightCornerTileIndexes.append(metatile.tilesOrder[1])
            bottomLeftCornerTileIndexes.append(metatile.tilesOrder[2])
            bottomRightCornerTileIndexes.append(metatile.tilesOrder[3])


        # Write metatile top left corner array
        metatileTopLeftTileIndexAssembly = "/Users/ricardo/Temp/metatileTopLeftTileIndexArray.inc"
        metatileTopLeftTileIndexAssembly = open(metatileTopLeftTileIndexAssembly, "w")
        metatileTopLeftTileIndexAssembly.write(".DB " + ",".join(str(index) for index in topLeftCornerTileIndexes) + "\n")
        metatileTopLeftTileIndexAssembly.close()

        # Write metatile top right corner array
        metatileTopRightTileIndexAssembly = "/Users/ricardo/Temp/metatileTopRightTileIndexArray.inc"
        metatileTopRightTileIndexAssembly = open(metatileTopRightTileIndexAssembly, "w")
        metatileTopRightTileIndexAssembly.write(
            ".DB " + ",".join(str(index) for index in topRightCornerTileIndexes) + "\n")
        metatileTopRightTileIndexAssembly.close()

        # Write metatile bottom left corner array
        metatileBottomLeftTileIndexAssembly = "/Users/ricardo/Temp/metatileBottomLeftTileIndexArray.inc"
        metatileBottomLeftTileIndexAssembly = open(metatileBottomLeftTileIndexAssembly, "w")
        metatileBottomLeftTileIndexAssembly.write(
            ".DB " + ",".join(str(index) for index in bottomLeftCornerTileIndexes) + "\n")
        metatileBottomLeftTileIndexAssembly.close()

        # Write metatile bottom right corner array
        metatileBottomRightTileIndexAssembly = "/Users/ricardo/Temp/metatileBottomRightTileIndexArray.inc"
        metatileBottomRightTileIndexAssembly = open(metatileBottomRightTileIndexAssembly, "w")
        metatileBottomRightTileIndexAssembly.write(
            ".DB " + ",".join(str(index) for index in bottomRightCornerTileIndexes) + "\n")
        metatileBottomRightTileIndexAssembly.close()



    # Returns a new list with ALL metatiles, including the ones included in parent metatiles.
    # And the returned list has them all sorted.
    def extractAllActualMetatilesAndSort(self, metatiles):
        allMetatiles = []

        for key, value in metatiles.items():
            allMetatiles.append(value)

            if value.yFlippedMetatile is not None:
                allMetatiles.append(value.yFlippedMetatile)

            if value.xFlippedMetatile is not None:
                allMetatiles.append(value.xFlippedMetatile)

            if value.xyFlippedMetatile is not None:
                allMetatiles.append(value.xyFlippedMetatile)

        sortedMetatiles = [None] * len(allMetatiles)
        for metatile in allMetatiles:
            sortedMetatiles[metatile.id] = metatile

        return sortedMetatiles



    def sortMaptiles(self, maptiles):
        sortedMaptiles = [None] * len(maptiles)

        for maptile in maptiles:
            sortedMaptiles[maptile.id] = maptile

        return sortedMaptiles


    def getBackground0(self, tilelayers):
        for layer in tilelayers:
            if layer[MaptileConverter.LAYER_NAME] == MaptileConverter.LAYER_NAME_BACKGROUND_0:
                return layer

    def getForeground0(self, tilelayers):
        for layer in tilelayers:
            if layer[MaptileConverter.LAYER_NAME] == MaptileConverter.LAYER_NAME_FOREGROUND_0:
                return layer



    def extractAndSortSlices(self, unsortedSlices):
        self.slices = []
        slicesAmount = len(unsortedSlices)

        for x in range(0, slicesAmount):
            for slice in unsortedSlices:
                if slice[MaptileConverter.SLICES_NAME] == str(x):
                    self.slices.append(slice)
                    break
