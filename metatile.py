from tile import Tile

class Metatile:
    INCREMENTING_ID = 0

    # Corner indexes
    TOP_LEFT = 0
    TOP_RIGHT = 1
    BOTTOM_LEFT = 2
    BOTTOM_RIGHT = 3

    def __init__(self):
        self.id = Metatile.INCREMENTING_ID
        Metatile.INCREMENTING_ID += 1

        # Metatiles composed of the same tile corners, but on different positions.
        self.yFlippedMetatile = None
        self.xFlippedMetatile = None
        self.xyFlippedMetatile = None

        # Contains the four corner tile instances.
        self.cornerTiles = []

        # Contains the indexes to the unique tiles.
        self.tilesOrder = []


    def appendPixelColour(self, rowIndex, columnIndex, paletteIndex):
        if rowIndex < 0 or rowIndex > 15:
            raise Exception("Row index for tile must be between 0 - 15. Index was {}", str(rowIndex))

        cornerIndex = self.getCornerIndex(rowIndex, columnIndex)

        try:
            cornerTile = self.cornerTiles[cornerIndex]
        except IndexError:
            cornerTile = Tile()
            self.cornerTiles.append(cornerTile)

        tileRowIndex = rowIndex

        if tileRowIndex > 7:
            tileRowIndex -= 8

        cornerTile.appendPixelColour(tileRowIndex, paletteIndex)


    def getCornerIndex(self, rowIndex, columnIndex):
        if rowIndex < 8 and columnIndex < 8:
            return Metatile.TOP_LEFT
        elif rowIndex < 8 and columnIndex >= 8:
            return Metatile.TOP_RIGHT
        if rowIndex >= 8 and columnIndex < 8:
            return Metatile.BOTTOM_LEFT
        elif rowIndex >= 8 and columnIndex >= 8:
            return Metatile.BOTTOM_RIGHT
        else:
            raise Exception("Incorrect rowIndex {}, or columnIndex {}", str(rowIndex), str(columnIndex))


    '''
    Returns the index to the metatile that is flipped in either or all directions.
    If a metatile that is flipped in the given directions has not previously been created, then this it
    will be created in this instance.
    '''
    def getMetatileIndex(self, horizontallyFlipped, verticallyFlipped):
        properMetatileIndex = -1

        if not horizontallyFlipped and not verticallyFlipped:
            properMetatileIndex = self.id
        elif horizontallyFlipped and verticallyFlipped:
            if self.xyFlippedMetatile is None:
                flippedMetatile = Metatile()
                reorderedCornerTiles = [self.cornerTiles[Metatile.BOTTOM_RIGHT],
                                        self.cornerTiles[Metatile.BOTTOM_LEFT],
                                        self.cornerTiles[Metatile.TOP_RIGHT],
                                        self.cornerTiles[Metatile.TOP_LEFT]]
                flippedMetatile.cornerTiles = reorderedCornerTiles

                reorderedTilesOrder = [self.tilesOrder[Metatile.BOTTOM_RIGHT],
                                        self.tilesOrder[Metatile.BOTTOM_LEFT],
                                        self.tilesOrder[Metatile.TOP_RIGHT],
                                        self.tilesOrder[Metatile.TOP_LEFT]]
                flippedMetatile.tilesOrder = reorderedTilesOrder

                self.xyFlippedMetatile = flippedMetatile
                properMetatileIndex = flippedMetatile.id
            else:
                properMetatileIndex = self.xyFlippedMetatile.id
        elif horizontallyFlipped:
            if self.xFlippedMetatile is None:
                flippedMetatile = Metatile()
                reorderedCornerTiles = [self.cornerTiles[Metatile.TOP_RIGHT],
                                        self.cornerTiles[Metatile.TOP_LEFT],
                                        self.cornerTiles[Metatile.BOTTOM_RIGHT],
                                        self.cornerTiles[Metatile.BOTTOM_LEFT]]
                flippedMetatile.cornerTiles = reorderedCornerTiles

                reorderedTilesOrder = [self.tilesOrder[Metatile.TOP_RIGHT],
                                       self.tilesOrder[Metatile.TOP_LEFT],
                                       self.tilesOrder[Metatile.BOTTOM_RIGHT],
                                       self.tilesOrder[Metatile.BOTTOM_LEFT]]
                flippedMetatile.tilesOrder = reorderedTilesOrder

                self.xFlippedMetatile = flippedMetatile
                properMetatileIndex = flippedMetatile.id
            else:
                properMetatileIndex = self.xFlippedMetatile.id
        elif verticallyFlipped:
            if self.yFlippedMetatile is None:
                flippedMetatile = Metatile()
                reorderedCornerTiles = [self.cornerTiles[Metatile.BOTTOM_LEFT],
                                        self.cornerTiles[Metatile.BOTTOM_RIGHT],
                                        self.cornerTiles[Metatile.TOP_LEFT],
                                        self.cornerTiles[Metatile.TOP_RIGHT]]
                flippedMetatile.cornerTiles = reorderedCornerTiles

                reorderedTilesOrder = [self.tilesOrder[Metatile.BOTTOM_LEFT],
                                       self.tilesOrder[Metatile.BOTTOM_RIGHT],
                                       self.tilesOrder[Metatile.TOP_LEFT],
                                       self.tilesOrder[Metatile.TOP_RIGHT]]
                flippedMetatile.tilesOrder = reorderedTilesOrder

                self.yFlippedMetatile = flippedMetatile
                properMetatileIndex = flippedMetatile.id
            else:
                properMetatileIndex = self.yFlippedMetatile.id

        return properMetatileIndex