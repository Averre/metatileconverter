class Palette():
    def __init__(self, pngPaletteContents):
        self.PALETTE_START_INDEX = 0
        self.MAX_PALETTE_SIZE = self.PALETTE_START_INDEX + 32

        self.RGBConversionTable = {}
        self.RGBConversionTable[0] = "00"
        self.RGBConversionTable[85] = "01"
        self.RGBConversionTable[170] = "10"
        self.RGBConversionTable[255] = "11"

        self.RED = 0
        self.GREEN = 1
        self.BLUE = 2

        self.palette = pngPaletteContents


    def colourToBinary(self, index):
        if self.PALETTE_START_INDEX > index > self.MAX_PALETTE_SIZE:
            raise Exception("Palette index must be between %i - %i", self.PALETTE_START_INDEX, self.MAX_PALETTE_SIZE)

        VDPBinaryFormat = "00"
        VDPBinaryFormat += self.RGBConversionTable[self.palette[index][self.BLUE]]
        VDPBinaryFormat += self.RGBConversionTable[self.palette[index][self.GREEN]]
        VDPBinaryFormat += self.RGBConversionTable[self.palette[index][self.RED]]

        return VDPBinaryFormat


    def colourToHex(self, index):
        if self.PALETTE_START_INDEX > index > self.MAX_PALETTE_SIZE:
            raise Exception("Palette index must be between %i - %i", self.PALETTE_START_INDEX, self.MAX_PALETTE_SIZE)

        binaryValue = self.colourToBinary(index)
        return "{:02x}".format(int(binaryValue, 2))


    def toVDPFormat(self):
        backgroundPalette = []
        spritePalette = []

        '''
        Convert RGB values to the format that the VDP uses.
        Binary: --BBGGRR

        The two first bits are unused.
        '''
        counter = 0
        for index in range(self.PALETTE_START_INDEX, self.MAX_PALETTE_SIZE):
            VDPHexFormat = self.colourToHex(index)

            if counter < 16:
                backgroundPalette.append(VDPHexFormat)
            else:
                spritePalette.append(VDPHexFormat)

            counter += 1

        paletteData = ".DB " + ",".join("$" + hexValue for hexValue in backgroundPalette) + "\n"
        paletteData += ".DB " + ",".join("$" + hexValue for hexValue in spritePalette) + "\n"

        return paletteData


    def sizeControl(self, width, height):
        if (int(width) * int(height)) % 8 != 0:
            raise Exception("Incorrect tilemap dimensions! - Width: " + width + "; height: " + height)