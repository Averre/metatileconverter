class Tile:
    def __init__(self):
        # Contains the tiles 8 rows, where each row contains 8 palette indexes to the pixels.
        self.rows = []
        self.index = -1


    def appendPixelColour(self, rowIndex, paletteIndex):
        if rowIndex < 0 or rowIndex > 7:
            raise Exception("Row index for tile must be between 0 - 7. Index was {}", str(rowIndex))

        row = None

        try:
            row = self.rows[rowIndex]
        except IndexError:
            row = []
            self.rows.append(row)

        row.append(paletteIndex)


    def getRow(self, rowIndex):
        return self.rows[rowIndex]


    def appendIndex(self, index):
        self.index = index


    def toVDPFormat(self):
        firstHexRowHalf = []
        secondHexRowHalf = []
        i = 0

        binRows = []

        for row in self.rows:
            binaryColourRows = []
            for paletteIndex in row:
                binaryColourRows.append("{:04b}".format(paletteIndex))

            binRows.append(binaryColourRows)

            for x in range(3, -1, -1):
                if i < 4:
                    firstHexRowHalf.append(self.combineRowColours(binaryColourRows, x))
                else:
                    secondHexRowHalf.append(self.combineRowColours(binaryColourRows, x))
            i += 1

        VDPFormat = ".DB " + ",".join("$" + hexValue for hexValue in firstHexRowHalf) + "\n"
        VDPFormat += ".DB " + ",".join("$" + hexValue for hexValue in secondHexRowHalf)

        return VDPFormat


    def combineRowColours(self, binaryRow, bitIndex):
        combinedBinaryRow = ""
        for pixel in binaryRow:
            combinedBinaryRow += pixel[bitIndex]

        return "{:02x}".format(int(combinedBinaryRow, 2))