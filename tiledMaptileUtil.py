class TiledMaptileUtil:

    # Tiled application specific values to find the id and flip flags.
    GID_MASK = 0x0FFFFFFF
    HORIZONTAL_MASK = 0x80000000
    VERTICAL_MASK = 0x40000000
    DIAGONAL_MASK = 0x20000000

    @staticmethod
    def getActualGlobalId(id):
        step1 = ((int(id) << 0) & 0x0FFFFFFF) | (id & ~0x0FFFFFFF)
        return (id & 0x0FFFFFFF)

    @staticmethod
    def isHorizontallyFlipped(id):
        return TiledMaptileUtil.maskProperty(id, TiledMaptileUtil.HORIZONTAL_MASK, 31)

    @staticmethod
    def isVerticallyFlipped(id):
        return TiledMaptileUtil.maskProperty(id, TiledMaptileUtil.VERTICAL_MASK, 30)

    @staticmethod
    def isDiagonallyFlipped(id):
        # Unsupported!
        # MapTile.maskProperty(id, self.DIAGONAL_MASK, 29)
        raise Exception("Diagonal flip is not supported!!!")

    @staticmethod
    def maskProperty(value, mask, shift):
        step1 = ((bool(value) << shift) & mask) | (value & ~mask)
        return bool(value & mask)

    @staticmethod
    def createModFlag(foreground, palette, verticallyFlipped, horizontallyFlipped):
        vFlipped = 0
        hFlipped = 0

        if verticallyFlipped:
            vFlipped = 1

        if horizontallyFlipped:
            hFlipped = 1

        modFlag = 0x00 | (foreground << 4)
        modFlag |= (palette << 3)
        modFlag |= (vFlipped << 2)
        modFlag |= (hFlipped << 1)

        return modFlag
